﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.IO;
using System.Security.Cryptography;

namespace aes
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private string _password = "Password123";
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.DefaultExt = ".txt";

            bool? result = op.ShowDialog();
            if(result.HasValue)
            {
                FileNameText.Text = op.FileName;
                using(StreamReader sr = new StreamReader(op.FileName))
                {
                    FileText.Text = sr.ReadToEnd();
                }
            }

        }

        private void EncryptButton_Click(object sender, RoutedEventArgs e)
        {
            byte[] salt = GenerateSalt();
            var localLink = FileNameText.Text.Split('.')[0] + ".aes";
            FileStream fs = new FileStream(localLink,FileMode.Create);
            byte[] passwordBytes = Encoding.Unicode.GetBytes(_password);

            RijndaelManaged aes = new RijndaelManaged();  
            aes.KeySize = 256;
            aes.BlockSize = 128;
            aes.Padding = PaddingMode.PKCS7;

            var key = new Rfc2898DeriveBytes(passwordBytes, salt, 50000);
            aes.Key = key.GetBytes(aes.KeySize / 8);
            aes.IV = key.GetBytes(aes.BlockSize / 8);
            aes.Mode = CipherMode.CFB;
            fs.Write(salt, 0, salt.Length);

            CryptoStream cs = new CryptoStream(fs, aes.CreateEncryptor(), CryptoStreamMode.Write);
            FileStream fsIn = new FileStream(FileNameText.Text,FileMode.Open);

            byte[] buffer = new byte[108576];
            int read;

            try
            {
                while((read = fsIn.Read(buffer,0,buffer.Length))>0)
                {
                     fsIn.Write(buffer, 0, read);
                }
            }
            catch(CryptographicException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                fs.Close();
                fsIn.Close();
                using (StreamReader sr = new StreamReader(localLink))
                {
                    EncryptedText.Text = sr.ReadToEnd();
                }
                if(File.Exists(FileNameText.Text))
                {
                    File.Delete(FileNameText.Text);
                    FileNameText.Text = String.Empty;
                }
            }
        }

        private byte[] GenerateSalt()
        {
           byte[] data = new byte[32];
            using (RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider())
            {
                for(int i = 0;i <10;i++)
                    rng.GetBytes(data);
                return data;
            }
        }
    }
}
